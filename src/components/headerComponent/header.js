import React from 'react';

const Header = () => {
    let url="https://www.education2conf.com/";
    return (
        
            <div className="header">
                <div className="container">
                    <nav className="navbar  main-navbar p-0"> 
                        <a href={url} className="navbar-brand"><img src ="../images/main_logo.png"  className="main-logo" alt="logo"/></a>
                        <button className="navbar-toggler navbar-toggler-expand-lg" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                    </nav>
                </div>
            </div>  
        
    )
}

export default Header
