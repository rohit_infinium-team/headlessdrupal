import React from 'react'

const Speaker = () => {
    return (
        <section className="speaker_section home_section section sec4">
            <div className="speaker_section_content">
                <div className="row ml-0 mr-0">
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6 pl-0 pr-0"></div>
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6 pl-0 pr-0">
                        <div className="speaker_figure_wrapper">
                            <figure className="speaker_figure">      
                                <img src="../images/speakers_img.jpg"  alt="img" width="100%"/>
                            </figure>                     
                        </div>
                        
                    </div>
                </div>
                <div className="speaker_text_area">
                    <div className="container">
                            <div className="row">
                                <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                    <h3>OUR SPEAKERS</h3>
                                    <h2>Be Inspired By<br/> These Pioneering<br/> <span>Speakers</span></h2>
                                    <p>Through its platform, the Education 2.0 Conference presents an enriching and memorable 
                                        experience to individuals and organizations who are continually striving hard to push 
                                        the frontiers of innovation in the diverse fields of education.</p>

                                        <button type="button" className="btn btn-md btn_view_all">
                                            view all
                                        </button>

                                    
                                </div>
                                
                            </div>
                    </div>  
                </div>
                
            </div>
        </section>
    )
}

export default Speaker
