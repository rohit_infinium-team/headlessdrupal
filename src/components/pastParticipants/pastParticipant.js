import React from 'react'

const PastParticipant = () => {
    return (
        <section className="past_attendee_section home_section section sec6">
            <div className="container">
                <div className="row mb-5">
                    <div className="col-xs-12 col-sm-12 col-md-8 col-lg-8 ">
                        
                        <h3>Who Should Attends</h3>
                        <h2>Enriching Education<br/> Through <span>Collaboration</span></h2>
                        <p>Through its platform, the Education 2.0 Conference presents an enriching and memorable experience to 
                            individuals and organizations who are continually striving hard to push the frontiers of innovation 
                            in the diverse fields of education.</p>
                    </div>
                </div>
                <div className="row mb-5">
                    <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2 ">
                        <figure className="past_attndee_figure">
                        <img src="../images/past_att_img.jpg" alt="img" className="attendee_slick_img" />
                        </figure>
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2 ">
                        <figure className="past_attndee_figure">
                        <img src="../images/past_att_img.jpg" alt="img" className="attendee_slick_img" />
                        </figure>
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2 ">
                        <figure className="past_attndee_figure">
                        <img src="../images/past_att_img.jpg" alt="img" className="attendee_slick_img" />
                        </figure>
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2 ">
                        <figure className="past_attndee_figure">
                        <img src="../images/past_att_img.jpg" alt="img" className="attendee_slick_img" />
                        </figure>
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2 ">
                        <figure className="past_attndee_figure">
                        <img src="../images/past_att_img.jpg" alt="img" className="attendee_slick_img" />
                        </figure>
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2 ">
                        <figure className="past_attndee_figure">
                        <img src="../images/past_att_img.jpg" alt="img" className="attendee_slick_img" />
                        </figure>
                    </div>
                </div>
                <div className="row ">
                    <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2 ">
                        <figure className="past_attndee_figure">
                        <img src="../images/past_att_img.jpg" alt="img" className="attendee_slick_img" />
                        </figure>
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2 ">
                        <figure className="past_attndee_figure">
                        <img src="../images/past_att_img.jpg" alt="img" className="attendee_slick_img" />
                        </figure>
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2 ">
                        <figure className="past_attndee_figure">
                        <img src="../images/past_att_img.jpg" alt="img" className="attendee_slick_img" />
                        </figure>
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2 ">
                        <figure className="past_attndee_figure">
                        <img src="../images/past_att_img.jpg" alt="img" className="attendee_slick_img" />
                        </figure>
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2 ">
                        <figure className="past_attndee_figure">
                        <img src="../images/past_att_img.jpg" alt="img" className="attendee_slick_img" />
                        </figure>
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2 ">
                        <figure className="past_attndee_figure">
                        <img src="../images/past_att_img.jpg" alt="img" className="attendee_slick_img" />
                        </figure>
                    </div>
                </div>
            </div>
            
        </section>
    )
}

export default PastParticipant
