import React from 'react';
import Slick from '../slickComponent/slick';


const WhyAttend = () => {
    return (
        <section className="why_attendee_section home_section section sec3">
            <div className="container">
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div className="why_attendee_text">
                            <h3>Why Attend?</h3>
                            <h2>Top Takeaways From <span>Education 2.0</span></h2>
                            <p>Leaders engaged in reshaping the education space are truly the backbone of society. At the Education 2.0 Conference</p>

                            <Slick/>
                            
                        </div>

                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <figure className="attendee_figure">
                            <img src="../images/attendee_img.jpg" alt="img" width="100%"/>
                        </figure>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default WhyAttend
