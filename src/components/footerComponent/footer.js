import React from 'react';
import { FaFacebookSquare, FaYoutube, FaLinkedin, FaTwitterSquare, FaInstagramSquare } from "react-icons/fa";
const footer = () => {
    let url="";
    return (
        <footer className="footer">
            <div className="container">
                <div className="footer_content">
                    <p>© 2021, All Rights Reserved.</p>
                    <ul className="secondery_social_list">
                        <li>
                            <a href={url}><FaFacebookSquare/></a>
                        </li>
                        <li>
                            <a href={url}><FaYoutube/></a>
                        </li>
                        <li>
                            <a href={url}><FaLinkedin/></a>
                        </li>
                        <li>
                            <a href={url}><FaTwitterSquare/></a>
                        </li>
                        <li>
                            <a href={url}><FaInstagramSquare/></a>
                        </li>
                      
                    </ul>
                </div>
            </div>
        </footer>
    )
}

export default footer
