import React from 'react'

const RegisterForm = () => {
    return (
        <section className="register_section home_section section sec7">
            <div className="container">
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                        <div className="form_text">
                            <h3>COLLABORATION</h3>
                            <h2>Let’s talk.</h2>
                            
                            <p>We’re a team of creatives who are excited about unique ideas and help digital 
                                and fin-tech companies to create amazing identity by crafting top-notch UI/UX.</p>
                            <img src="../images/form_text_img.png" alt="img" />
                        </div>
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                        <div className="register_wrapper text-center">
                            <p>Register yourself, and get your seats now <i class="fab fa-accusoft"></i></p>
                            <form className="register_form_wrap">
                                <div className="row g-2 mb-3">
                                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <input type="text" name="name" className=" form-control input_name input_field" placeholder="Name*"/>
                                    </div>
                                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <input type="email" name="email" className=" form-control input_email input_field" placeholder="Email*"/>
                                    </div>
                                </div>
                                <div className="row g-2 mb-3">
                                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <input type="tel" name="tel" className="form-control input_tel input_field" placeholder="Phone"/>
                                    </div>
                                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <input type="text" name="subject" className="form-control input_subject input_field" placeholder="Subject"/>
                                    </div>
                                </div>
                                <div className="row g-2 mb-3">
                                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                                    </div>
                                   
                                </div>
                                <div class="form-check mb-3 text-center">
                                   
                                    <label class="form-check-label" for="flexCheckDefault">
                                    <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault"/>
                                    I agree that my submited data is being collected and stored.
                                    </label>
                                </div>

                                <div class="col-12 text-center">
                                    <button class="btn btn-primary" type="submit">Submit form</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default RegisterForm
