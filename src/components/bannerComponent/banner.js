import React from 'react';


const Banner = () => {
    let url="";
    return (  
            <section className="banner_section home_section section sec1">
                    <figure className="banner_figure">
                        <img src="../images/banner.jpg" alt="banner-img" width="100%" />
                        <figcaption className="banner_caption">
                            <div className="container">
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                        <div className="banner_caption_caption">
                                            <h1>Education 2.0 <br/> Conference</h1>
                                            <h3>Uniting Changemakers To Revolutionize Education</h3>
                                            <p>A three-day, knowledge-intensive conference, the Education 2.0 attracts the most dynamic 
                                                leaders in the education sector and promises to bring fresh insights and perspectives to 
                                                empower, inspire, and enrich the global learning community.
                                            </p>
                                            <div className="banner_conferance_detail">
                                                <button className="btn btn-md banner_conf_btn">Dubai, UAE  |  Feb 14-16, 2022</button>
                                                <button className="btn btn-md banner_conf_btn">Las Vegas, USA  |  March 15th-17th, 2022</button>
                                            </div>
                                            <div className="banner_conferance_reg_log">
                                                <a href = {url}>register</a>
                                                <a href = {url}><img src="../images/banner_login_icon.png" alt="login-img" /> login</a>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </figcaption>
                    </figure>

                    
                
            </section>
        
    )
}

export default Banner
