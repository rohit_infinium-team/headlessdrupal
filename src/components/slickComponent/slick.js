import React from 'react';
import '../../../node_modules/slick-carousel/slick/slick.css';
import '../../../node_modules/slick-carousel/slick/slick-theme.css';
import Slider from '../../../node_modules/react-slick/dist/react-slick';

const Slick = () =>{
  
      const settings = {
        dots: true,
        infinite: true,
        speed: 2000,
        slidesToShow: 1,
        autoplay: true,
        autoplaySpeed: 4000,
        slidesToScroll: 1
        
      }
      return (
        <div className="home_slick_slider">
          <Slider {...settings}>
            <div className="slider-content">
              <h3>Networking Opportunities</h3>
              <p>Gathering the most renowned educationists, EdTech innovators, policymakers, 
                  and researchers, the Education 2.0 Conference reimagines the education space 
                  and looks for innovative solutions to the most pressing problems impacting learning and teaching on a global level.</p>
            </div>
            <div className="slider-content">
              <h3>Networking Opportunities 2</h3>
              <p>Gathering the most renowned educationists, EdTech innovators, policymakers, 
                  and researchers, the Education 2.0 Conference reimagines the education space 
                  and looks for innovative solutions to the most pressing problems impacting learning and teaching on a global level.</p>
            </div>
            <div className="slider-content">
              <h3>Networking Opportunities 3 </h3>
              <p>Gathering the most renowned educationists, EdTech innovators, policymakers, 
                  and researchers, the Education 2.0 Conference reimagines the education space 
                  and looks for innovative solutions to the most pressing problems impacting learning and teaching on a global level.</p>
            </div>
            <div className="slider-content">
              <h3>Networking Opportunities 4</h3>
              <p>Gathering the most renowned educationists, EdTech innovators, policymakers, 
                  and researchers, the Education 2.0 Conference reimagines the education space 
                  and looks for innovative solutions to the most pressing problems impacting learning and teaching on a global level.</p>
            </div>
            
          </Slider>
        </div>
      );
    }
  

export default Slick
