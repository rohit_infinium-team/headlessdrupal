import React from 'react'
import '../../../node_modules/slick-carousel/slick/slick.css';
import '../../../node_modules/slick-carousel/slick/slick-theme.css';
import Slider from '../../../node_modules/react-slick/dist/react-slick';
const WhoShouldAtt = () => {
    const settings = {
        dots: true,
        infinite: true,
        speed: 2000,
        slidesToShow: 3,
        autoplay: true,
        autoplaySpeed: 4000,
        slidesToScroll: 1
        
      }
    return (
        <section className="who_should_att_section home_section section sec5">
            <div className="container">
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-8 col-lg-8 ">
                        
                        <h3>Who Should Attends</h3>
                        <h2>Enriching Education<br/> Through <span>Collaboration</span></h2>
                        <p>Through its platform, the Education 2.0 Conference presents an enriching and memorable experience to 
                            individuals and organizations who are continually striving hard to push the frontiers of innovation 
                            in the diverse fields of education.</p>
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                        <div className="attendee_slick">
                            <Slider {...settings}>
                                <div className="slider-content">
                                    <figure className="attendee_slick_figure">
                                        <img src="../images/attendee_slick_icon.png" alt="img" className="attendee_slick_img" />
                                    </figure>  
                                
                                    <h3>Networking Opportunities</h3>
                                    <p>Gathering the most renowned educationists, EdTech innovators, policymakers, 
                                    and researchers, the Education 2.0 Conference reimagines the education space 
                                    and looks for innovative solutions to the most pressing problems impacting learning and teaching on a global level.</p>
                                </div>
                                <div className="slider-content">
                                    <figure className="attendee_slick_figure">
                                        <img src="../images/attendee_slick_icon2.png" alt="img" className="attendee_slick_img" />
                                    </figure>
                                    <h3>Networking Opportunities 2</h3>
                                    <p>Gathering the most renowned educationists, EdTech innovators, policymakers, 
                                        and researchers, the Education 2.0 Conference reimagines the education space 
                                    and looks for innovative solutions to the most pressing problems impacting learning and teaching on a global level.</p>
                                </div>
                                <div className="slider-content">
                                    <figure className="attendee_slick_figure">
                                        <img src="../images/attendee_slick_icon3.png" alt="img" className="attendee_slick_img" />
                                    </figure>
                                    <h3>Networking Opportunities 3 </h3>
                                    <p>Gathering the most renowned educationists, EdTech innovators, policymakers, 
                                    and researchers, the Education 2.0 Conference reimagines the education space 
                                    and looks for innovative solutions to the most pressing problems impacting learning and teaching on a global level.</p>
                                </div>
                                <div className="slider-content">
                                    <figure className="attendee_slick_figure">
                                        <img src="../images/attendee_slick_icon.png" alt="img" className="attendee_slick_img" />
                                    </figure>
                                    <h3>Networking Opportunities 4</h3>
                                    <p>Gathering the most renowned educationists, EdTech innovators, policymakers, 
                                    and researchers, the Education 2.0 Conference reimagines the education space 
                                    and looks for innovative solutions to the most pressing problems impacting learning and teaching on a global level.</p>
                                </div>
                                
                            </Slider>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default WhoShouldAtt
