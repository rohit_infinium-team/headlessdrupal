import React from 'react'

const WhoWeAre = () => {
   
    return (
        <section className="who_we_are_section home_section section sec2">
            <div className="who_we_are_section_content">
                <div className="container">
                    <div className="row">
                        <div className="col-12 col-sm-12 col-md-7 col-lg-7">
                            <h3>ABOUT THE EDUCATION 2.0 CONFERENCE</h3>
                            <h2>Enriching Education<br/>Through <span>Collaboration</span></h2>
                            <p>Gathering the most renowned educationists, EdTech innovators, policymakers, and researchers, the Education 2.0 Conference 
                                reimagines the education space and looks for innovative solutions to the most pressing problems impacting learning and 
                                teaching on a global level. It aspires to offer its stage to education leaders with inspiring stories, journeys and 
                                perspectives and promote networking and collaboration between like-minded peers. The conference will also spotlight 
                                disruptive technologies, tools and practices that have the potential to reshape the future of learning as we know it.</p>

                                <p>Given the critical role of education in empowering young minds, raising the quality of lives of millions around the
                                     world and creating a better, brighter tomorrow, we hope to fuel dialogue and drive change in this sector at our 
                                     much-awaited education event.</p>

                            <ul className="attendee_list">
                                <li>
                                    <strong>400+</strong>
                                    ATTENDEES
                                </li>
                                <li>
                                    <strong>40+</strong>
                                    SESSIONS
                                </li>
                                <li>
                                    <strong>20+</strong>
                                    SPEAKERS
                                </li>
                                <li>
                                    <strong>30+</strong>
                                    EXHIBITORS
                                </li>
                            </ul>
                        </div>
                        <div className="col-12 col-sm-12 col-md-5 col-lg-5">

                            <figure className="whowefigure">
                                <img src="../images/who_we_r_img.png"  alt="img" width="100"/>
                            </figure>

                        </div>
                    </div>
                </div>  
            </div>
        </section>
    )
}

export default WhoWeAre
