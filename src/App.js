import React from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import './assets/css/pagepiling.css';
import './assets/css/style.css'; 
import $ from 'jquery';
import './assets/js/jquery.pagepiling';
import Header from './components/headerComponent/header';
import Banner from './components/bannerComponent/banner';
import WhoWeAre from './components/whoWeAreComponent/whoWeAre';
import WhyAttend from './components/whyAttend/whyAttend';
import Speaker from './components/speakersComponent/speaker';
import WhoShouldAtt from './components/whoShouldAttendComponent/whoShouldAtt';
import PastParticipant from './components/pastParticipants/pastParticipant';
import RegisterForm from './components/registerComponent/registerForm';
import Footer from './components/footerComponent/footer';


const App = () => {
  $(document).ready(function() {
    
    $('#pagepiling').pagepiling({
          menu: false,
          direction: 'vertical',
          verticalCentered: true,
          sectionsColor: [],
          anchors: ['sec1', 'sec2', 'sec3', 'sec4', 'sec5', 'sec6','sec7'],
          scrollingSpeed: 700,
          easing: 'swing',
          loopBottom: false,
          loopTop: false,
          css3: true,
          navigation: {
              'textColor': '#fff',
              'bulletsColor': '#fff',
              'position': 'right',
              'tooltips': ['sec 1', 'sec 2', 'sec 3', 'sec 4', 'sec 5', 'sec 6','sec 7']
              
          },
          normalScrollElements: true,
          normalScrollElementTouchThreshold: 7,
          touchSensitivity: 7,
          keyboardScrolling: true,
          sectionSelector: '.section',
          animateAnchor: false,
          //events
          onLeave: function(index, nextIndex, direction){},
        afterLoad: function(anchorLink, index){},
        afterRender: function(){},
        
    });
    
  });
  return (
    
    <div className="content">
      <Header />
      <div id="pagepiling">       
        <Banner />
        <WhoWeAre />
        <WhyAttend/>
        <Speaker/>
        <WhoShouldAtt/>
        <PastParticipant/>
        <RegisterForm/>
      </div>
      <Footer/>
    </div>

  )
}

export default App

